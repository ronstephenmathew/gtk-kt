package org.gtk.gdk

import gtk.GdkDragCancelReason
import gtk.GdkDragCancelReason.*

/**
 * 03 / 01 / 2022
 *
 * @see <a href=""></a>
 */
enum class DragCancelReason(
	val gdk: GdkDragCancelReason,
) {
	NO_TARGET(GDK_DRAG_CANCEL_NO_TARGET),
	USER_CANCELLED(GDK_DRAG_CANCEL_USER_CANCELLED),
	ERROR(GDK_DRAG_CANCEL_ERROR);

	companion object {
		fun valueOf(gdk: GdkDragCancelReason) =
			when (gdk) {
				GDK_DRAG_CANCEL_NO_TARGET -> NO_TARGET
				GDK_DRAG_CANCEL_USER_CANCELLED -> USER_CANCELLED
				GDK_DRAG_CANCEL_ERROR -> ERROR
			}
	}
}