package org.gtk.gdk

import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString
import org.gtk.gdk.Texture.Companion.wrap
import org.gtk.gobject.KGObject

/**
 * kotlinx-gtk
 *
 * 28 / 07 / 2021
 *
 * @see <a href="https://docs.gtk.org/gdk4/class.Cursor.html">GdkCursor</a>
 */
class Cursor(val cursorPointer: CPointer<GdkCursor>) : KGObject(cursorPointer.reinterpret()) {

	/**
	 * @see <a href="https://docs.gtk.org/gdk4/ctor.Cursor.new_from_name.html">gdk_cursor_new_from_name</a>
	 */
	constructor(name: String, fallback: Cursor?) : this(gdk_cursor_new_from_name(name, fallback?.cursorPointer)!!)

	/**
	 * @see <a href="https://docs.gtk.org/gdk4/ctor.Cursor.new_from_texture.html">gdk_cursor_new_from_texture</a>
	 */
	constructor(texture: Texture, hotspotX: Int, hotspotY: Int, fallback: Cursor?) : this(
		gdk_cursor_new_from_texture(
			texture.texturePointer,
			hotspotX,
			hotspotY,
			fallback?.cursorPointer
		)!!
	)

	/**
	 * @see <a href="https://docs.gtk.org/gdk4/method.Cursor.get_fallback.html">gdk_cursor_get_fallback</a>
	 */
	val fallback: Cursor?
		get() = gdk_cursor_get_fallback(cursorPointer).wrap()

	/**
	 * @see <a href="https://docs.gtk.org/gdk4/method.Cursor.get_hotspot_x.html">gdk_cursor_get_hotspot_x</a>
	 */
	val hotspotX: Int
		get() = gdk_cursor_get_hotspot_x(cursorPointer)

	/**
	 * @see <a href="https://docs.gtk.org/gdk4/method.Cursor.get_hotspot_y.html">gdk_cursor_get_hotspot_y</a>
	 */
	val hotspotY: Int
		get() = gdk_cursor_get_hotspot_y(cursorPointer)

	/**
	 * @see <a href="https://docs.gtk.org/gdk4/method.Cursor.get_name.html">gdk_cursor_get_name</a>
	 */
	val name: String?
		get() = gdk_cursor_get_name(cursorPointer)?.toKString()

	/**
	 * @see <a href="https://docs.gtk.org/gdk4/method.Cursor.get_texture.html">gdk_cursor_get_texture</a>
	 */
	val texture: Texture?
		get() = gdk_cursor_get_texture(cursorPointer).wrap()

	companion object {
		inline fun CPointer<GdkCursor>?.wrap() =
			this?.wrap()

		inline fun CPointer<GdkCursor>.wrap() =
			Cursor(this)
	}
}