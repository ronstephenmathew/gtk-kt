package org.gtk.gtk.sorter

import gtk.*
import kotlinx.cinterop.reinterpret
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gtk.Expression
import org.gtk.gtk.Expression.Companion.wrap

/**
 * 17 / 12 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.StringSorter.html">
 *     GtkStringSorter</a>
 */
class StringSorter(val stringSorterPointer: GtkStringSorter_autoptr) :
	Sorter(stringSorterPointer.reinterpret()) {

	constructor(expression: Expression) : this(gtk_string_sorter_new(expression.expressionPointer)!!)

	var expression: Expression?
		get() = gtk_string_sorter_get_expression(stringSorterPointer).wrap()
		set(value) = gtk_string_sorter_set_expression(
			stringSorterPointer,
			value?.expressionPointer
		)

	var ignoreCase: Boolean
		get() = gtk_string_sorter_get_ignore_case(stringSorterPointer).bool
		set(value) = gtk_string_sorter_set_ignore_case(stringSorterPointer, value.gtk)
}