package org.gtk.gtk.widgets.windows

import gtk.GTK_TYPE_DIALOG
import gtk.GtkDialog
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.widgets.Widget

/**
 * kotlinx-gtk
 * 08 / 03 / 2021
 * TODO gtkx
 */
class Plug(
	@Suppress("MemberVisibilityCanBePrivate")
	 val plugPointer: CPointer<GtkDialog>
) : Window(plugPointer.reinterpret()) {

	constructor(widget: Widget) : this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_DIALOG))
}