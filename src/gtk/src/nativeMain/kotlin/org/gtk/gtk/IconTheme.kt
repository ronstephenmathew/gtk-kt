package org.gtk.gtk

import glib.gpointer
import gobject.GCallback
import gtk.*
import kotlinx.cinterop.*
import org.gtk.gdk.Display
import org.gtk.gdk.Display.Companion.wrap
import org.gtk.gio.Icon
import org.gtk.glib.bool
import org.gtk.glib.freeToArray
import org.gtk.gobject.KGObject
import org.gtk.gobject.Signals
import org.gtk.gobject.addSignalCallback
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.IconPaintable.Companion.wrap
import org.gtk.gobject.TypedNoArgFunc
import org.gtk.gtk.common.enums.TextDirection
import org.gtk.gtk.widgets.Widget

/**
 * 17 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.IconTheme.html">
 *     GtkIconTheme</a>
 */
class IconTheme(
	val iconThemePointer: GtkIconTheme_autoptr,
) : KGObject(iconThemePointer.reinterpret()) {

	constructor() : this(gtk_icon_theme_new()!!)

	constructor(widget: Widget) : this(typeCheckInstanceCastOrThrow(
		widget,
		GTK_TYPE_ICON_THEME
	))

	fun addResourcePath(path: String) {
		gtk_icon_theme_add_resource_path(
			iconThemePointer,
			path
		)
	}

	fun addSearchPath(path: String) {
		gtk_icon_theme_add_search_path(
			iconThemePointer,
			path
		)
	}

	val display: Display?
		get() = gtk_icon_theme_get_display(iconThemePointer).wrap()

	val iconNames: Array<String>
		get() = gtk_icon_theme_get_icon_names(iconThemePointer)!!.freeToArray()

	fun getIconSizes(iconName: String): Array<Int> =
		gtk_icon_theme_get_icon_sizes(
			iconThemePointer,
			iconName
		)!!.freeToArray()

	var resourcePaths: Array<String>?
		get() =
			gtk_icon_theme_get_resource_path(iconThemePointer)?.freeToArray()
		set(value) = memScoped {
			gtk_icon_theme_set_resource_path(
				iconThemePointer,
				value?.toCStringArray(this)
			)
		}

	var searchPaths: Array<String>?
		get() = gtk_icon_theme_get_search_path(iconThemePointer)?.freeToArray()
		set(value) = memScoped {
			gtk_icon_theme_set_search_path(
				iconThemePointer,
				value?.toCStringArray(this)
			)
		}

	var themeName: String
		get() = gtk_icon_theme_get_theme_name(iconThemePointer)!!.toKString()
		set(value) = gtk_icon_theme_set_theme_name(iconThemePointer, value)

	fun hasIcon(icon: Icon) =
		gtk_icon_theme_has_gicon(iconThemePointer, icon.iconPointer).bool

	fun hasIcon(iconName: String) =
		gtk_icon_theme_has_icon(iconThemePointer, iconName).bool

	fun lookupByIcon(
		icon: Icon,
		size: Int,
		scale: Int,
		direction: TextDirection,
		flags: GtkIconLookupFlags,
	): IconPaintable = gtk_icon_theme_lookup_by_gicon(
		iconThemePointer,
		icon.iconPointer,
		size,
		scale,
		direction.gtk,
		flags
	)!!.wrap()

	fun lookupIcon(
		iconName: String,
		fallbacks: Array<String>,
		size: Int,
		scale: Int,
		direction: TextDirection,
		flags: GtkIconLookupFlags,
	): IconPaintable = memScoped {
		gtk_icon_theme_lookup_icon(
			iconThemePointer,
			iconName,
			fallbacks.toCStringArray(this),
			size,
			scale,
			direction.gtk,
			flags
		)!!.wrap()
	}

	fun addOnChangedCallback(action: TypedNoArgFunc<IconTheme>) =
		addSignalCallback(Signals.CHANGED, action, staticNoArgFunc)

	companion object {
		inline fun GtkIconTheme_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkIconTheme_autoptr.wrap() =
			IconTheme(this)

		fun getForDisplay(display: Display): IconTheme =
			gtk_icon_theme_get_for_display(display.displayPointer)!!.wrap()


		private val staticNoArgFunc: GCallback =
			staticCFunction {
					self: GtkIconTheme_autoptr,
					data: gpointer,
				->
				data.asStableRef<TypedNoArgFunc<IconTheme>>()
					.get()
					.invoke(self.wrap())
				Unit
			}.reinterpret()
	}
}