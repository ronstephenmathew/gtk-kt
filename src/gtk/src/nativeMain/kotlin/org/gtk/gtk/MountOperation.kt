package org.gtk.gtk

import gtk.*
import kotlinx.cinterop.reinterpret
import org.gtk.gdk.Display
import org.gtk.gdk.Display.Companion.wrap
import org.gtk.glib.bool
import org.gtk.gtk.widgets.windows.Window
import org.gtk.gtk.widgets.windows.Window.Companion.wrap

/**
 * 20 / 12 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/ctor.MountOperation.new.html">
 *     GtkMountOperation</a>
 */
class MountOperation(
	val mountOperationPointer: GtkMountOperation_autoptr,
) : org.gtk.gio.MountOperation(mountOperationPointer.reinterpret()) {

	constructor(parent: Window?) :
			this(gtk_mount_operation_new(parent?.windowPointer)!!.reinterpret())

	var display: Display
		get() = gtk_mount_operation_get_display(mountOperationPointer)!!.wrap()
		set(value) = gtk_mount_operation_set_display(mountOperationPointer, value.displayPointer)

	var parent: Window?
		get() = gtk_mount_operation_get_parent(mountOperationPointer).wrap()
		set(value) = gtk_mount_operation_set_parent(mountOperationPointer, value?.windowPointer)

	val isShowing: Boolean
		get() = gtk_mount_operation_is_showing(mountOperationPointer).bool
}