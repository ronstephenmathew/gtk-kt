package org.gtk.gtk.common.data

data class PreferredWidth(
	val minimumWidth: Int,
	val naturalWidth: Int,
)