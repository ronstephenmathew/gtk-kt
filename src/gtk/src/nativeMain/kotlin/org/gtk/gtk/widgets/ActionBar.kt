package org.gtk.gtk.widgets

import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.Accessible
import org.gtk.gtk.Buildable
import org.gtk.gtk.ConstraintTarget

/**
 * kotlinx-gtk
 *
 * 13 / 03 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.ActionBar.html">GtkActionBar</a>
 */
open class ActionBar(
	val actionBarPointer: CPointer<GtkActionBar>,
) : Widget(actionBarPointer.reinterpret()), Accessible, Buildable, ConstraintTarget {


	/**
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.ActionBar.new.html">
	 *     gtk_action_bar_new</a>
	 */
	constructor() : this(gtk_action_bar_new()!!.reinterpret())

	constructor(widget: Widget) :
			this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_ACTION_BAR))

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.ActionBar.get_center_widget.html">
	 *     gtk_action_bar_get_center_widget</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.ActionBar.set_center_widget.html">
	 *     gtk_action_bar_set_center_widget</a>
	 */
	var centerWidget: Widget?
		get() = gtk_action_bar_get_center_widget(actionBarPointer)?.let {
			Widget(it)
		}
		set(value) = gtk_action_bar_set_center_widget(
			actionBarPointer,
			value?.widgetPointer
		)

	var revealed: Boolean
		get() = gtk_action_bar_get_revealed(actionBarPointer).bool
		set(value) = gtk_action_bar_set_revealed(actionBarPointer, value.gtk)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.ActionBar.pack_end.html">
	 *     gtk_action_bar_pack_end</a>
	 */
	fun packEnd(child: Widget) {
		gtk_action_bar_pack_end(actionBarPointer, child.widgetPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.ActionBar.pack_start.html">
	 *     gtk_action_bar_pack_start</a>
	 */
	fun packStart(child: Widget) {
		gtk_action_bar_pack_start(actionBarPointer, child.widgetPointer)
	}

	fun remove(child: Widget) {
		gtk_action_bar_remove(actionBarPointer, child.widgetPointer)
	}

	companion object {

		fun CPointer<GtkActionBar>?.wrap() =
			this?.wrap()

		fun CPointer<GtkActionBar>.wrap() =
			ActionBar(this)
	}
}