package org.gtk.gtk.widgets

import gtk.GtkWindowHandle_autoptr
import gtk.gtk_window_handle_get_child
import gtk.gtk_window_handle_new
import gtk.gtk_window_handle_set_child
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.KGObject
import org.gtk.gtk.widgets.Widget.Companion.wrap

/**
 * gtk-kt
 *
 * 13 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.WindowHandle.html">GtkWindowHandle</a>
 */
class WindowHandle(val windowHandlePointer: GtkWindowHandle_autoptr) :
	KGObject(windowHandlePointer.reinterpret()) {

	constructor() : this(gtk_window_handle_new()!!.reinterpret())

	var child: Widget?
		get() = gtk_window_handle_get_child(windowHandlePointer).wrap()
		set(value) = gtk_window_handle_set_child(windowHandlePointer, value?.widgetPointer)
}