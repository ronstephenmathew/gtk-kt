package org.gtk.gtk.action

import gtk.GTK_TYPE_NAMED_ACTION
import gtk.GtkNamedAction_autoptr
import gtk.gtk_named_action_get_action_name
import gtk.gtk_named_action_new
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString
import org.gtk.gobject.TypeInstance
import org.gtk.gobject.typeCheckInstanceCastOrThrow

/**
 * gtk-kt
 *
 * 20 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.NamedAction.html">
 *     GtkNamedAction</a>
 */
class NamedAction(val namedAction: GtkNamedAction_autoptr) :
	ShortcutAction(namedAction.reinterpret()) {

	constructor(name: String) : this(gtk_named_action_new(name)!!.reinterpret())

	constructor(obj: TypeInstance) :
			this(typeCheckInstanceCastOrThrow(obj, GTK_TYPE_NAMED_ACTION))

	val actionName: String
		get() = gtk_named_action_get_action_name(namedAction)!!.toKString()

	companion object {

		inline fun GtkNamedAction_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkNamedAction_autoptr.wrap() =
			NamedAction(this)
	}
}