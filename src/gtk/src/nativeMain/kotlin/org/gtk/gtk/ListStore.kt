package org.gtk.gtk

import gtk.*
import kotlinx.cinterop.memScoped
import kotlinx.cinterop.reinterpret
import org.gtk.glib.bool
import org.gtk.glib.toCArray
import org.gtk.gobject.KGObject
import org.gtk.gobject.KGType
import org.gtk.gobject.KGType.Companion.toCArray
import org.gtk.gobject.Value

/**
 * @see <a href="https://docs.gtk.org/gtk4/class.ListStore.html">
 *     GtkListStore</a>
 */
class ListStore constructor(
	val listStorePointer: GtkListStore_autoptr,
) : KGObject(listStorePointer.reinterpret()), TreeModel {

	override val treeModelPointer: GtkTreeModel_autoptr by lazy {
		listStorePointer.reinterpret()
	}

	constructor(vararg types: KGType) : this(
		memScoped {
			gtk_list_store_newv(
				types.size,
				types.toCArray(this)
			)!!
		}
	)

	fun append(iter: TreeIter) {
		gtk_list_store_append(listStorePointer, iter.treeIterPointer)
	}

	fun clear() {
		gtk_list_store_clear(listStorePointer)
	}

	fun insert(iter: TreeIter, position: Int) {
		gtk_list_store_insert(listStorePointer, iter.treeIterPointer, position)
	}

	fun insertBefore(iter: TreeIter, sibling: TreeIter) {
		gtk_list_store_insert_before(listStorePointer,
			iter.treeIterPointer,
			sibling.treeIterPointer)
	}

	fun insertAfter(iter: TreeIter, sibling: TreeIter) {
		gtk_list_store_insert_after(
			listStorePointer,
			iter.treeIterPointer,
			sibling.treeIterPointer
		)
	}

	// TODO possibly ignore gtk_list_store_insert_with_valuesv ?

	fun isValid(iter: TreeIter): Boolean =
		gtk_list_store_iter_is_valid(
			listStorePointer,
			iter.treeIterPointer
		).bool

	fun moveAfter(iter: TreeIter, position: TreeIter) {
		gtk_list_store_move_after(
			listStorePointer,
			iter.treeIterPointer,
			position.treeIterPointer
		)
	}

	fun moveBefore(iter: TreeIter, position: TreeIter) {
		gtk_list_store_move_before(
			listStorePointer,
			iter.treeIterPointer,
			position.treeIterPointer
		)
	}

	fun prepend(iter: TreeIter) {
		gtk_list_store_prepend(listStorePointer, iter.treeIterPointer)
	}

	fun remove(iter: TreeIter) {
		gtk_list_store_remove(listStorePointer, iter.treeIterPointer)
	}

	fun reorder(newOrder: Array<Int>) {
		memScoped {
			gtk_list_store_reorder(
				listStorePointer,
				newOrder.toCArray(this)
			)
		}
	}

	// ignore gtk_list_store_set vararg

	fun setColumnTypes(types: Array<KGType>) {
		memScoped {
			gtk_list_store_set_column_types(
				listStorePointer, types.size, types.toCArray(this)
			)
		}
	}


	// TODO gtk_list_store_set_valist

	fun setValue(iter: TreeIter, column: Int, value: Value?) {
		gtk_list_store_set_value(
			listStorePointer,
			iter.treeIterPointer,
			column,
			value?.pointer
		)
	}

	// TODO gtk_list_store_set_valuesv

	fun swap(a: TreeIter, b: TreeIter) {
		gtk_list_store_swap(
			listStorePointer,
			a.treeIterPointer,
			b.treeIterPointer
		)
	}
}