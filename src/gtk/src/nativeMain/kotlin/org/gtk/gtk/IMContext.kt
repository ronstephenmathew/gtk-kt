package org.gtk.gtk

import gtk.GtkIMContext_autoptr
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.KGObject

/**
 * 06 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.IMContext.html">
 *     GtkIMContext</a>
 */
open class IMContext(
	val imContextPointer: GtkIMContext_autoptr,
) : KGObject(imContextPointer.reinterpret()) {

	companion object{

		inline fun GtkIMContext_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkIMContext_autoptr.wrap() =
			IMContext(this)
	}
}