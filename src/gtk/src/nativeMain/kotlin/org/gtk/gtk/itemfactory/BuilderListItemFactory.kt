package org.gtk.gtk.itemfactory

import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString
import org.gtk.glib.KGBytes
import org.gtk.glib.KGBytes.Companion.wrap
import org.gtk.gobject.TypeInstance
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.BuilderScope
import org.gtk.gtk.BuilderScope.Companion.wrap

/**
 * 27 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.BuilderListItemFactory.html">
 *     GtkBuilderListItemFactory</a>
 */
class BuilderListItemFactory(
	val builderListItemFactoryPointer: CPointer<GtkBuilderListItemFactory>,
) : ListItemFactory(builderListItemFactoryPointer.reinterpret()) {

	constructor(obj: TypeInstance) : this(typeCheckInstanceCastOrThrow(
		obj,
		GTK_TYPE_BUILDER_LIST_ITEM_FACTORY
	))

	constructor(
		scope: BuilderScope,
		bytes: KGBytes,
	) : this(gtk_builder_list_item_factory_new_from_bytes(
		scope.builderScopePointer,
		bytes.pointer
	)!!.reinterpret())

	constructor(
		scope: BuilderScope,
		resourcePath: String,
	) : this(gtk_builder_list_item_factory_new_from_resource(
		scope.builderScopePointer,
		resourcePath
	)!!.reinterpret())

	val bytes: KGBytes
		get() = gtk_builder_list_item_factory_get_bytes(
			builderListItemFactoryPointer
		)!!.wrap()

	val resource: String?
		get() = gtk_builder_list_item_factory_get_resource(
			builderListItemFactoryPointer
		)?.toKString()

	val scope: BuilderScope
		get() = gtk_builder_list_item_factory_get_scope(
			builderListItemFactoryPointer
		)!!.wrap()
}