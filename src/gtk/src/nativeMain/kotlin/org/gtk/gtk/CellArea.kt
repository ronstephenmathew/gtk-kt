package org.gtk.gtk

import glib.gboolean
import glib.gpointer
import gobject.GCallback
import gobject.GValue
import gtk.*
import kotlinx.cinterop.*
import org.gtk.gdk.Event
import org.gtk.gdk.Rectangle
import org.gtk.gdk.Rectangle.Companion.wrap
import org.gtk.glib.*
import org.gtk.glib.WrappedKList.Companion.asWrappedKList
import org.gtk.gobject.*
import org.gtk.gobject.Value.Companion.wrap
import org.gtk.gtk.CellAreaContext.Companion.wrap
import org.gtk.gtk.CellEditable.Companion.wrap
import org.gtk.gtk.TreeIter.Companion.wrap
import org.gtk.gtk.TreeModel.Companion.wrap
import org.gtk.gtk.cellrenderer.CellRenderer
import org.gtk.gtk.cellrenderer.CellRenderer.Companion.wrap
import org.gtk.gtk.common.data.PreferredHeight
import org.gtk.gtk.common.data.PreferredWidth
import org.gtk.gtk.common.enums.DirectionType
import org.gtk.gtk.common.enums.Orientation
import org.gtk.gtk.common.enums.SizeRequestMode
import org.gtk.gtk.widgets.Widget

/**
 * 13 / 03 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.CellArea.html">
 *     GtkCellArea</a>
 */
open class CellArea(
	val cellAreaPointer: GtkCellArea_autoptr,
) : InitiallyUnowned(cellAreaPointer.reinterpret()), Buildable, CellLayout {
	override val buildablePointer: GtkBuildable_autoptr by lazy {
		cellAreaPointer.reinterpret()
	}

	override val cellLayoutHolder: GtkCellLayout_autoptr by lazy {
		cellAreaPointer.reinterpret()
	}

	constructor(widget: Widget) :
			this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_CELL_AREA))

	fun activate(
		context: CellAreaContext,
		widget: Widget,
		cellArea: Rectangle,
		flags: GtkCellRendererState,
		editOnly: Boolean,
	): Boolean =
		gtk_cell_area_activate(
			cellAreaPointer,
			context.cellAreaContextPointer,
			widget.widgetPointer,
			cellArea.rectanglePointer,
			flags,
			editOnly.gtk
		).bool

	fun activateCell(
		widget: Widget,
		renderer: CellRenderer,
		event: Event,
		cellArea: Rectangle,
		flags: GtkCellRendererState,
	): Boolean =
		gtk_cell_area_activate_cell(
			cellAreaPointer,
			widget.widgetPointer,
			renderer.cellRendererPointer,
			event.eventPointer,
			cellArea.rectanglePointer,
			flags
		).bool

	fun add(renderer: CellRenderer) {
		gtk_cell_area_add(cellAreaPointer, renderer.cellRendererPointer)
	}

	fun addFocusSibling(renderer: CellRenderer, sibling: CellRenderer) {
		gtk_cell_area_add_focus_sibling(
			cellAreaPointer,
			renderer.cellRendererPointer,
			sibling.cellRendererPointer
		)
	}

	// ignore gtk_cell_area_add_with_properties vararg

	fun applyAttributes(
		treeModel: TreeModel,
		iter: TreeIter,
		isExpander: Boolean,
		isExpanded: Boolean,
	) {
		gtk_cell_area_apply_attributes(
			cellAreaPointer,
			treeModel.treeModelPointer,
			iter.treeIterPointer,
			isExpander.gtk,
			isExpanded.gtk
		)
	}

	fun attributeConnect(
		renderer: CellRenderer,
		attribute: String,
		column: Int,
	) {
		gtk_cell_area_attribute_connect(
			cellAreaPointer,
			renderer.cellRendererPointer,
			attribute,
			column
		)
	}

	fun attributeDisconnect(
		renderer: CellRenderer,
		attribute: String,
	) {
		gtk_cell_area_attribute_disconnect(
			cellAreaPointer,
			renderer.cellRendererPointer,
			attribute,
		)
	}

	fun attributeGetColumn(renderer: CellRenderer, attribute: String): Int =
		gtk_cell_area_attribute_get_column(
			cellAreaPointer,
			renderer.cellRendererPointer,
			attribute
		)

	// ignore gtk_cell_area_cell_get vararg

	fun cellGetProperty(renderer: CellRenderer, propertyName: String): Value =
		memScoped {
			val value = alloc<GValue>()

			gtk_cell_area_cell_get_property(
				cellAreaPointer,
				renderer.cellRendererPointer,
				propertyName,
				value.ptr
			)

			value.ptr.wrap()
		}

	// ignore gtk_cell_area_cell_get_valist strange API

	// ignore gtk_cell_area_cell_set vararg

	fun cellSetProperty(
		renderer: CellRenderer,
		propertyName: String,
		value: Value,
	) {
		gtk_cell_area_cell_set_property(
			cellAreaPointer,
			renderer.cellRendererPointer,
			propertyName,
			value.pointer
		)
	}

	// ignore gtk_cell_area_cell_set_valist strange API

	fun copyContext(context: CellAreaContext): CellAreaContext =
		gtk_cell_area_copy_context(
			cellAreaPointer,
			context.cellAreaContextPointer
		)!!.wrap()

	fun createContext(): CellAreaContext =
		gtk_cell_area_create_context(cellAreaPointer)!!.wrap()

	fun event(
		context: CellAreaContext,
		widget: Widget,
		event: Event,
		cellArea: Rectangle,
		flags: GtkCellRendererState,
	): Int =
		gtk_cell_area_event(
			cellAreaPointer,
			context.cellAreaContextPointer,
			widget.widgetPointer,
			event.eventPointer,
			cellArea.rectanglePointer,
			flags
		)

	fun focus(direction: DirectionType): Boolean =
		gtk_cell_area_focus(cellAreaPointer, direction.gtk).bool

	fun forEach(func: CellCallback) {
		StableRef.create(func).usePointer {
			gtk_cell_area_foreach(
				cellAreaPointer,
				staticCellCallback,
				it
			)
		}
	}

	fun forEachAlloc(
		context: CellAreaContext,
		widget: Widget,
		cellArea: Rectangle,
		backgroundArea: Rectangle,
		func: CellAllocCallback,
	) {
		StableRef.create(func).usePointer {
			gtk_cell_area_foreach_alloc(
				cellAreaPointer,
				context.cellAreaContextPointer,
				widget.widgetPointer,
				cellArea.rectanglePointer,
				backgroundArea.rectanglePointer,
				staticCellAllocCallback,
				it
			)
		}
	}

	fun getCellAllocation(
		context: CellAreaContext,
		widget: Widget,
		renderer: CellRenderer,
		cellArea: Rectangle,
	): Rectangle = memScoped {
		val allocation = alloc<GdkRectangle>()

		gtk_cell_area_get_cell_allocation(
			cellAreaPointer,
			context.cellAreaContextPointer,
			widget.widgetPointer,
			renderer.cellRendererPointer,
			cellArea.rectanglePointer,
			allocation.ptr
		)

		allocation.ptr.wrap()
	}

	fun getCellAtPosition(
		context: CellAreaContext,
		widget: Widget,
		cellArea: Rectangle,
		x: Int,
		y: Int,
		allocArea: Rectangle,
	): CellRenderer =
		gtk_cell_area_get_cell_at_position(
			cellAreaPointer,
			context.cellAreaContextPointer,
			widget.widgetPointer,
			cellArea.rectanglePointer,
			x,
			y,
			allocArea.rectanglePointer
		)!!.wrap()

	val currentPathString: String
		get() = gtk_cell_area_get_current_path_string(
			cellAreaPointer
		)!!.toKString()

	val editWidget: CellEditable
		get() = gtk_cell_area_get_edit_widget(cellAreaPointer)!!.wrap()

	val editedCell: CellRenderer
		get() = gtk_cell_area_get_edited_cell(cellAreaPointer)!!.wrap()

	val focusCell: CellRenderer
		get() = gtk_cell_area_get_focus_cell(cellAreaPointer)!!.wrap()

	fun getFocusFromSibling(renderer: CellRenderer): CellRenderer? =
		gtk_cell_area_get_focus_from_sibling(
			cellAreaPointer,
			renderer.cellRendererPointer
		).wrap()

	fun getFocusSiblings(renderer: CellRenderer): WrappedKList<CellRenderer> =
		gtk_cell_area_get_focus_siblings(
			cellAreaPointer,
			renderer.cellRendererPointer
		)!!.asWrappedKList(
			{ reinterpret<GtkCellRenderer>().wrap() },
			{ cellRendererPointer }
		)


	fun getPreferredHeight(
		context: CellAreaContext,
		widget: Widget,
	): PreferredHeight =
		memScoped {
			val minimumHeight = cValue<IntVar>()
			val naturalHeight = cValue<IntVar>()

			gtk_cell_area_get_preferred_height(
				cellAreaPointer,
				context.cellAreaContextPointer,
				widget.widgetPointer,
				minimumHeight,
				naturalHeight
			)

			PreferredHeight(
				minimumHeight.ptr.pointed.value,
				naturalHeight.ptr.pointed.value
			)
		}

	fun getPreferredHeightForWidth(
		context: CellAreaContext,
		widget: Widget,
		width: Int,
	): PreferredHeight =
		memScoped {
			val minimumHeight = cValue<IntVar>()
			val naturalHeight = cValue<IntVar>()

			gtk_cell_area_get_preferred_height_for_width(
				cellAreaPointer,
				context.cellAreaContextPointer,
				widget.widgetPointer,
				width,
				minimumHeight,
				naturalHeight,
			)

			PreferredHeight(
				minimumHeight.ptr.pointed.value,
				naturalHeight.ptr.pointed.value
			)
		}


	fun getPreferredWidth(
		context: CellAreaContext,
		widget: Widget,
	): PreferredWidth =
		memScoped {
			val minimumWidth = cValue<IntVar>()
			val naturalWidth = cValue<IntVar>()

			gtk_cell_area_get_preferred_width(
				cellAreaPointer,
				context.cellAreaContextPointer,
				widget.widgetPointer,
				minimumWidth,
				naturalWidth
			)

			PreferredWidth(
				minimumWidth.ptr.pointed.value,
				naturalWidth.ptr.pointed.value
			)
		}

	fun getPreferredWidthForHeight(
		context: CellAreaContext,
		widget: Widget,
		height: Int,
	): PreferredWidth =
		memScoped {
			val minimumWidth = cValue<IntVar>()
			val naturalWidth = cValue<IntVar>()

			gtk_cell_area_get_preferred_width_for_height(
				cellAreaPointer,
				context.cellAreaContextPointer,
				widget.widgetPointer,
				height,
				minimumWidth,
				naturalWidth,
			)

			PreferredWidth(
				minimumWidth.ptr.pointed.value,
				naturalWidth.ptr.pointed.value
			)
		}

	val requestMode: SizeRequestMode
		get() = SizeRequestMode.valueOf(
			gtk_cell_area_get_request_mode(cellAreaPointer)
		)

	fun hasRenderer(renderer: CellRenderer): Boolean =
		gtk_cell_area_has_renderer(
			cellAreaPointer,
			renderer.cellRendererPointer
		).bool

	fun innerCellArea(
		widget: Widget,
		cellArea: Rectangle,
	): Rectangle =
		memScoped {
			val innerArea = alloc<GdkRectangle>()

			gtk_cell_area_inner_cell_area(
				cellAreaPointer,
				widget.widgetPointer,
				cellArea.rectanglePointer,
				innerArea.ptr
			)

			innerArea.ptr.wrap()
		}

	val isActivatable: Boolean
		get() = gtk_cell_area_is_activatable(cellAreaPointer).bool

	fun isFocusSibling(renderer: CellRenderer, sibling: CellRenderer): Boolean =
		gtk_cell_area_is_focus_sibling(
			cellAreaPointer,
			renderer.cellRendererPointer,
			sibling.cellRendererPointer
		).bool

	fun remove(renderer: CellRenderer) {
		gtk_cell_area_remove(cellAreaPointer, renderer.cellRendererPointer)
	}

	fun removeFocusSibling(renderer: CellRenderer, sibling: CellRenderer) {
		gtk_cell_area_remove_focus_sibling(
			cellAreaPointer,
			renderer.cellRendererPointer,
			sibling.cellRendererPointer
		)
	}

	data class SpecialSize(
		val minimumSize: Int,
		val naturalSize: Int,
	)

	fun requestRenderer(
		renderer: CellRenderer,
		orientation: Orientation,
		widget: Widget,
		forSize: Int,
	): SpecialSize = memScoped {
		val minimumSize = cValue<IntVar>()
		val naturalSize = cValue<IntVar>()

		gtk_cell_area_request_renderer(
			cellAreaPointer,
			renderer.cellRendererPointer,
			orientation.gtk,
			widget.widgetPointer,
			forSize,
			minimumSize,
			naturalSize
		)

		SpecialSize(
			minimumSize.ptr.pointed.value,
			naturalSize.ptr.pointed.value
		)
	}

	fun setFocusCell(renderer: CellRenderer) {
		gtk_cell_area_set_focus_cell(
			cellAreaPointer,
			renderer.cellRendererPointer
		)
	}

	fun snapshot(
		context: CellAreaContext,
		widget: Widget,
		snapshot: Snapshot,
		backgroundArea: Rectangle,
		cellArea: Rectangle,
		flags: GtkCellRendererState,
		paintFocus: Boolean,
	) {
		gtk_cell_area_snapshot(
			cellAreaPointer,
			context.cellAreaContextPointer,
			widget.widgetPointer,
			snapshot.snapshotPointer,
			backgroundArea.rectanglePointer,
			cellArea.rectanglePointer,
			flags,
			paintFocus.gtk
		)
	}

	fun stopEditing(canceled: Boolean) {
		gtk_cell_area_stop_editing(cellAreaPointer, canceled.gtk)
	}

	fun addOnAddEditableCallback(action: CellAreaAddEditableFunc) =
		addSignalCallback(
			Signals.ADD_EDITABLE,
			action,
			staticAddEditableCallback
		)

	fun addOnApplyAttributesCallback(action: CellAreaApplyAttributesFunc) =
		addSignalCallback(
			Signals.APPLY_ATTRIBUTES,
			action,
			staticApplyAttributesCallback
		)

	fun addOnFocusChangedCallback(action: CellAreaFocusChangedFunc) =
		addSignalCallback(
			Signals.FOCUS_CHANGED,
			action,
			staticFocusChangedCallback
		)

	fun addOnRemoveEditableCallback(action: CellAreaRemoveEditableFunc) =
		addSignalCallback(
			Signals.REMOVE_EDITABLE,
			action,
			staticRemoveEditableCallback
		)

	companion object {
		inline fun GtkCellArea_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkCellArea_autoptr.wrap() =
			CellArea(this)

		val staticCellCallback: GtkCellCallback =
			staticCFunction {
					renderer: GtkCellRenderer_autoptr?,
					data: gpointer?,
				->
				data!!.asStableRef<CellCallback>()
					.get()
					.invoke(renderer!!.wrap())
					.gtk
			}


		val staticCellAllocCallback: GtkCellAllocCallback =
			staticCFunction {
					renderer: GtkCellRenderer_autoptr?,
					cell_area: CPointer<GdkRectangle>?,
					cell_background: CPointer<GdkRectangle>?,
					data: gpointer?,
				->
				data!!.asStableRef<CellAllocCallback>()
					.get()
					.invoke(
						renderer!!.wrap(),
						cell_area!!.wrap(),
						cell_background!!.wrap()
					)
					.gtk
			}

		val staticAddEditableCallback: GCallback =
			staticCFunction {
					self: GtkCellArea_autoptr,
					renderer: GtkCellRenderer_autoptr,
					editable: GtkCellEditable_autoptr,
					cell_area: CPointer<GdkRectangle>,
					path: CStringPointer,
					data: gpointer,
				->
				data.asStableRef<CellAreaAddEditableFunc>()
					.get()
					.invoke(
						self.wrap(),
						renderer.wrap(),
						editable.wrap(),
						cell_area.wrap(),
						path.toKString()
					)
			}.reinterpret()

		val staticApplyAttributesCallback: GCallback =
			staticCFunction {
					self: GtkCellArea_autoptr,
					model: GtkTreeModel_autoptr,
					iter: GtkTreeIter_autoptr,
					is_expander: gboolean,
					is_expanded: gboolean,
					data: gpointer,
				->
				data.asStableRef<CellAreaApplyAttributesFunc>()
					.get()
					.invoke(
						self.wrap(),
						model.wrap(),
						iter.wrap(),
						is_expander.bool,
						is_expanded.bool
					)
			}.reinterpret()

		val staticFocusChangedCallback: GCallback =
			staticCFunction {
					self: GtkCellArea_autoptr,
					renderer: GtkCellRenderer_autoptr,
					path: CStringPointer,
					data: gpointer,
				->
				data.asStableRef<CellAreaFocusChangedFunc>()
					.get()
					.invoke(
						self.wrap(),
						renderer.wrap(),
						path.toKString()
					)
			}.reinterpret()

		val staticRemoveEditableCallback: GCallback =
			staticCFunction {
					self: GtkCellArea_autoptr,
					renderer: GtkCellRenderer_autoptr,
					editable: GtkCellEditable_autoptr,
					data: gpointer,
				->
				data.asStableRef<CellAreaRemoveEditableFunc>()
					.get()
					.invoke(
						self.wrap(),
						renderer.wrap(),
						editable.wrap()
					)
			}.reinterpret()

	}
}

typealias CellCallback = (renderer: CellRenderer) -> Boolean

typealias CellAllocCallback = (
	renderer: CellRenderer,
	cellArea: Rectangle,
	cellBackground: Rectangle,
) -> Boolean

typealias CellAreaAddEditableFunc =
		CellArea.(
			renderer: CellRenderer,
			editable: CellEditable,
			cellArea: Rectangle,
			path: String,
		) -> Unit

typealias CellAreaApplyAttributesFunc =
		CellArea.(
			model: TreeModel,
			iter: TreeIter,
			isExpander: Boolean,
			isExpanded: Boolean,
		) -> Unit

typealias CellAreaFocusChangedFunc =
		CellArea.(
			renderer: CellRenderer,
			path: String,
		) -> Unit

typealias CellAreaRemoveEditableFunc =
		CellArea.(
			renderer: CellRenderer,
			editable: CellEditable,
		) -> Unit