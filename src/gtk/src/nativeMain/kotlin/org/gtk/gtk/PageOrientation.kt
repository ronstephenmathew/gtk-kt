package org.gtk.gtk

import gtk.GtkPageOrientation
import gtk.GtkPageOrientation.*

/**
 * 05 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gtk4/enum.PageOrientation.html">
 *     GtkPageOrientation</a>
 */
enum class PageOrientation(
	val gtk: GtkPageOrientation,
) {
	PORTRAIT(GTK_PAGE_ORIENTATION_PORTRAIT),
	LANDSCAPE(GTK_PAGE_ORIENTATION_LANDSCAPE),
	REVERSE_PORTRAIT(GTK_PAGE_ORIENTATION_REVERSE_PORTRAIT),
	REVERSE_LANDSCAPE(GTK_PAGE_ORIENTATION_REVERSE_LANDSCAPE);

	companion object {
		fun valueOf(gtk: GtkPageOrientation) =
			when (gtk) {
				GTK_PAGE_ORIENTATION_PORTRAIT -> PORTRAIT
				GTK_PAGE_ORIENTATION_LANDSCAPE -> LANDSCAPE
				GTK_PAGE_ORIENTATION_REVERSE_PORTRAIT -> REVERSE_PORTRAIT
				GTK_PAGE_ORIENTATION_REVERSE_LANDSCAPE -> REVERSE_LANDSCAPE
			}
	}
}