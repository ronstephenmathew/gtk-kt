package org.gtk.gtk.controller.gesture.single

import glib.gpointer
import gobject.GCallback
import gtk.GTK_TYPE_GESTURE_CLICK
import gtk.GdkEventSequence
import gtk.GtkGestureClick_autoptr
import gtk.gtk_gesture_click_new
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction
import org.gtk.gdk.EventSequence
import org.gtk.gobject.Signals
import org.gtk.gobject.TypeInstance
import org.gtk.gobject.addSignalCallback
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gobject.TypedNoArgFunc

/**
 * gtk-kt
 *
 * 26 / 08 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.GestureClick.html"></a>
 */
class GestureClick(
	val gestureClickPointer: GtkGestureClick_autoptr,
) :
	GestureSingle(gestureClickPointer.reinterpret()) {

	constructor() : this(gtk_gesture_click_new()!!.reinterpret())

	constructor(obj: TypeInstance) :
			this(typeCheckInstanceCastOrThrow(obj, GTK_TYPE_GESTURE_CLICK))

	fun addOnPressedCallback(action: GestureClickForceFunction) =
		addSignalCallback(Signals.PRESSED, action, staticGestureForceFunction)

	fun addOnReleasedCallback(action: GestureClickForceFunction) =
		addSignalCallback(Signals.RELEASED, action, staticGestureForceFunction)

	fun addOnStoppedCallback(action: TypedNoArgFunc<GestureStylus>) =
		addSignalCallback(Signals.STOPPED, action, staticNoArgGCallback)

	fun addOnUnpairedReleaseCallback(
		action: GestureClickUnpairedReleaseFunction,
	) = addSignalCallback(
		Signals.UNPAIRED_RELEASE,
		action,
		staticGestureClickUnpairedReleaseFunction
	)

	companion object {

		inline fun GtkGestureClick_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkGestureClick_autoptr.wrap() =
			GestureClick(this)

		private val staticNoArgGCallback: GCallback =
			staticCFunction { self: GtkGestureClick_autoptr, data: gpointer ->
				data.asStableRef<TypedNoArgFunc<GestureClick>>()
					.get()
					.invoke(self.wrap())
				Unit
			}.reinterpret()

		private val staticGestureForceFunction: GCallback =
			staticCFunction {
					self: GtkGestureClick_autoptr,
					nPress: Int,
					x: Double,
					y: Double,
					data: gpointer,
				->
				data.asStableRef<GestureClickForceFunction>()
					.get()
					.invoke(
						self.wrap(),
						nPress,
						x,
						y
					)
			}.reinterpret()

		private val staticGestureClickUnpairedReleaseFunction: GCallback =
			staticCFunction {
					self: GtkGestureClick_autoptr,
					x: Double,
					y: Double,
					button: UInt,
					sequence: CPointer<GdkEventSequence>,
					data: gpointer,
				->
				data.asStableRef<GestureClickUnpairedReleaseFunction>().get().invoke(
					self.wrap(),
					x,
					y,
					button,
					EventSequence(sequence)
				)
			}.reinterpret()
	}
}

typealias GestureClickForceFunction =
		GestureClick.(
			nPress: Int,
			x: Double,
			y: Double,
		) -> Unit

typealias GestureClickUnpairedReleaseFunction =
		GestureClick.(
			x: Double,
			y: Double,
			button: UInt,
			sequence: EventSequence,
		) -> Unit