package org.gtk.gtk;

import gtk.GTK_ORDERING_EQUAL
import gtk.GTK_ORDERING_LARGER
import gtk.GTK_ORDERING_SMALLER
import gtk.GtkOrdering

/**
 * @see <a href="https://docs.gtk.org/gtk4/enum.SorterOrder.html">GtkSorterOrder</a>
 */
enum class Ordering(val gtk: GtkOrdering) {
	SMALL(GTK_ORDERING_SMALLER),
	EQUAL(GTK_ORDERING_EQUAL),
	LARGER(GTK_ORDERING_LARGER);

	companion object {
		inline fun valueOf(gtk: GtkOrdering) = values().find { it.gtk == gtk }
	}
}