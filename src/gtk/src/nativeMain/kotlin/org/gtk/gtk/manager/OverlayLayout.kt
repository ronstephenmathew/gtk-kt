package org.gtk.gtk.manager

import gtk.GtkOverlayLayout_autoptr
import gtk.gtk_overlay_layout_new
import kotlinx.cinterop.reinterpret

/**
 * kotlinx-gtk
 *
 * 22 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.OverlayLayout.html">
 *     GtkOverlayLayout</a>
 */
class OverlayLayout(
	val overlayLayoutPointer: GtkOverlayLayout_autoptr
) : LayoutManager(overlayLayoutPointer.reinterpret()) {

	constructor() : this(gtk_overlay_layout_new()!!.reinterpret())
}