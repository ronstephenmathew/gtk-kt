package org.gtk.gobject

import gobject.GParamSpec_autoptr
import kotlinx.cinterop.reinterpret

/**
 * 25 / 12 / 2021
 *
 * @see <a href=""></a>
 */
class ParamSpec(
	val paramSpecPointer: GParamSpec_autoptr,
) : KGObject(paramSpecPointer.reinterpret()) {

	companion object{
		inline fun GParamSpec_autoptr?.wrap() =
			this?.wrap()

		inline fun GParamSpec_autoptr.wrap() =
			ParamSpec(this)
	}
}