package org.gtk.glib

import glib.*
import kotlinx.cinterop.*

/**
 * kotlinx-gtk
 *
 * 13 / 04 / 2021
 *
 * @see <a href="https://docs.gtk.org/glib/struct.Error.html">GError</a>
 */
open class KGError(
	val pointer: CPointer<GError>
) : Exception(
	pointer.pointed.message?.toKString()
) {
	/**
	 * @see <a href="https://docs.gtk.org/glib/ctor.Error.new_literal.html">
	 *     g_error_new_literal</a>
	 */
	constructor(domain: UInt, code: Int, message: String) : this(
		g_error_new_literal(
			domain,
			code,
			message
		)!!
	)

	val code: Int by lazy {
		pointer.pointed.code
	}

	val domain: UInt by lazy {
		pointer.pointed.domain
	}

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.Error.copy.html">
	 *     g_error_copy</a>
	 */
	fun copy(): KGError = KGError(g_error_copy(pointer)!!)

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.Error.free.html">
	 *     g_error_free</a>
	 */
	fun free() {
		g_error_free(pointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.Error.matches.html">
	 *     g_error_matches</a>
	 */
	fun matches(domain: UInt, code: Int): Boolean =
		g_error_matches(pointer, domain, code).bool

	companion object {
		inline fun CPointer<GError>?.wrap() =
			this?.wrap()

		inline fun CPointer<GError>.wrap() =
			KGError(this)
	}
}

inline fun NativePlacement.allocateGErrorPtr() = allocPointerTo<GError>().ptr

/**
 * kotlinx-gtk
 * 08 / 04 / 2021
 *
 * @param throwException Default true, When true throws an exception,
 * otherwise returns the exception
 * @param extraMapping Allows extra types of [KGError] to be created,
 * for more specific kotlin exception catching
 */
@Throws(KGError::class)
fun CPointer<CPointerVar<GError>>.unwrap(
	throwException: Boolean = true,
	extraMapping: Map<UInt, (CPointer<GError>) -> KGError> = mapOf()
): Exception? {
	val err = pointed.pointed ?: return null
	val exception = when (err.domain) {
		else -> KGError(err.ptr)
	}

	if (throwException)
		throw exception
	else
		return exception
}
