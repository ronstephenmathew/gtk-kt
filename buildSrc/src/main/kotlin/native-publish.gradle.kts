import java.io.FileInputStream
import java.util.*

/**
 * 19 / 01 / 2022
 *
 * @see <a href=""></a>
 */
plugins {
	kotlin("multiplatform")
	`maven-publish`
	signing
}

fun loadPropertiesFile(path: String): Properties? {
	val propertiesFile = rootProject.file(path)

	return if (propertiesFile.exists()) {
		Properties().apply {
			load(FileInputStream(propertiesFile))
		}
	} else {
		println("No such properties file: $path")
		null
	}
}

val releasesRepoUrl =
	uri("https://s01.oss.sonatype.org/service/local/staging/deploy/maven2")

val snapshotsRepoUrl =
	uri("https://s01.oss.sonatype.org/content/repositories/snapshots/")

kotlin {
	publishing {

		publications {
			publications.filterIsInstance<MavenPublication>().forEach {
				it.artifactId =
					it.artifactId.replace("-kt", "")

				it.pom {
					name.set(project.name)
					description.set(project.description)
					url.set("https://gitlab.com/gtk-kt/gtk-kt")
					licenses {
						license {
							name.set("GNU Affero General Public License")
							url.set("https://www.gnu.org/licenses/agpl-3.0.html")
						}
					}
					developers {
						developer {
							id.set("doomsdayrs")
							name.set("Doomsdayrs")
							email.set("doomsdayrs@gmail.com")
						}
					}
					scm {
						connection.set("scm:git:git://gitlab.com/gtk-kt/gtk-kt.git")
						connection.set("scm:git:ssh://gitlab.com/gtk-kt/gtk-kt.git")
						url.set("https://gitlab.com/gtk-kt/gtk-kt")
					}
				}
			}

			repositories {
				maven {
					url =
						if (version.toString().endsWith("SNAPSHOT")) {
							snapshotsRepoUrl
						} else {
							releasesRepoUrl
						}

					val mavenProperties =
						loadPropertiesFile("./maven.properties")

					if (mavenProperties != null) {
						credentials {
							username =
								mavenProperties.getProperty("SONATYPE_USERNAME")
							password =
								mavenProperties.getProperty("SONATYPE_PASSWORD")
						}
					} else {
						println("No `maven.properties` file")
					}
				}
			}
		}
	}

	val signingProperties = loadPropertiesFile("./signing.properties")

	if (signingProperties != null) {
		project.ext.set(
			"signing.gnupg.keyName",
			signingProperties["signing.gnupg.keyName"]
		)
		signing {
			useGpgCmd()
			sign(publishing.publications)
		}
	} else {
		println("No `signing.properties` file")
	}
}