## gtk-kt

is a [GTK](https://gtk.org/) library for [Kotlin/Native](https://kotlinlang.org/docs/native-overview.html), providing
bindings for GTK, wrapping GTK to make it more accessible for Kotlin and offering a DSL making it easier to define UI.

### Dependency

Click on the modules below to see the dependency you have to add to your Gradle project.

### Community

The source code is at home on [GitLab](https://gitlab.com/gtk-kt/gtk-kt). For discussion, you can join
the [Matrix Space](https://matrix.to/#/#gtk-kt:matrix.org).
